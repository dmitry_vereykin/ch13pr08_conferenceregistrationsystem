/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import javax.swing.*;
import java.awt.*;

public class DinnerPanel extends JPanel {
    public final double DINNER = 30;

    private JCheckBox dinner;

    public DinnerPanel() {
        setLayout(new GridLayout(2, 1));

        dinner = new JCheckBox("Opening night dinner");

        setBorder(BorderFactory.createTitledBorder("Optional dinner"));

        add(dinner);
        add(new JLabel("Exclusively today! Only $30"));
    }

    public double getAddCost() {
        double addCost = 0.0;

        if (dinner.isSelected())
            addCost += DINNER;

        return addCost;
    }
}